CREATE DATABASE IF NOT EXISTS news_site;
Use news_site;

CREATE TABLE IF NOT EXISTS news (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  description TEXT(3000) NOT NULL,
  image VARCHAR(40),
  date_supply DATETIME NOT NULL 
);

CREATE TABLE IF NOT EXISTS comments (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  news_id INT NOT NULL,
  CONSTRAINT FK_news
  FOREIGN KEY (news_id)
  REFERENCES news (id)
  ON DELETE CASCADE        
  ON UPDATE CASCADE,
  author VARCHAR(45),
  description VARCHAR(255) NOT NULL
);

  INSERT INTO news (title, description, date_supply)
  VALUES ("NASA successfully collected a sample from asteroid Bennu", "The mission team analyzed images Thursday taken of the collector head of the spacecraft that showed that a substantial sample was collected -- but there is so much material in the head that the flap designed to keep the sample inside is jammed.",
  "2020-01-01 10:10:10"), 
  ("Sam Querrey", "American tennis player Sam Querrey is under scrutiny for leaving Russia on a private jet after testing positive for Covid-19.",
  "2020-05-09 12:00:05");
   
  INSERT INTO comments (news_id, author, description)
  VALUES (2, "Tom Holland", "Don't be sick, Sam"),
  (1, "Robert", "Very nice...");

