const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', async (req, res) => {
        try {
            const comments = await db.getItems('comments');
            const arr = comments.map(comment => {
                if (!comment.author) {
                    comment.author = "Anonymous"
                }
                return {'id': comment.id, 'news_id': comment.news_id, 'author': comment.author, 'comment': comment.description}
            });
            res.send(arr);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.post('/', async (req, res) => {
        let comment = req.body;
        if (!comment.news_id || !comment.description) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        console.log(comment);
        const newComment = await db.createItem('comments', comment);
        res.send(newComment);
    });

    router.delete('/:id', async (req, res) => {
        try {
            const response = await db.deleteItem('comments', req.params.id);
            res.send(response[0]);
        } catch (e) {
            res.send('Cannot be deleted')
        }
    });

    return router;
};

module.exports = createRouter;